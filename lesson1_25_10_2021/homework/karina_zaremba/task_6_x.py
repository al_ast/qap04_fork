from task_5_x import Human

human1 = Human()
km = 5
#human1.walk(km) - это вариант из 6*. Убрала для наглядности 7**

if km>=5:
    print('Sorry. I can’t walk so much')
else:
    print("I went " + str(km) + " kms")