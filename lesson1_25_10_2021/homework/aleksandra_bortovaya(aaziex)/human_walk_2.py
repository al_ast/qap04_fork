'''
7**. Модифицировать задачу 6 так:
Если кол-то километров больше 5, то вывести сообщение :
“Sorry. I can’t walk so much”
Если кол-во километров 5 и меньше, то оставить сообщение прежнем.
Добавить комментарий, что вы использовали для решения задачи
'''

class Human2:

    def walk(self, km):                             # if statement was used to solve the task
        if km > 5:                                  # condition
            print('Sorry. I can’t walk so much')    # statement that will be executed if the condition is true
        else:
            print(f'I went {km} kms')               # statement that will be executed if the condition is false