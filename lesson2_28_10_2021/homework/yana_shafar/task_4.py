
def check_number(number):
    try:
        number = int(number)

        if number >= 0:
            if number % 2 == 0:
                print("exact number is even")
            else:
                print("exact number is odd")


        else:
            print("exact number is negative")

    except ValueError:
        print("Only numbers are supported")


input_number = input("Input number:")
check_number(input_number)

