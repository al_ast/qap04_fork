# Task3*
# Read user input from the console
# For negative number print: “exact number is negative”
# For positive number print whether it is odd or even: e.g “exact number is odd”
# For strings print: “Only numbers are supported”

user_input = (input("Provide a number here: "))
try:
    number = int(user_input)
    if number >= 0:
        if number % 2 == 0:
            print("exact number is even")
        else:
            print("exact number is odd")
    else:
        print("exact number is negative")
except ValueError:
    print("Only numbers are supported")